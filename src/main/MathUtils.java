package main;

import java.util.Arrays;

public class MathUtils {

    public static int[] toto = {4,3,6,1,6};


    /**
     * @param n : the value to test
     * @param m : the value to test with
     * @return : boolean value
     */

    public static boolean isMultiple (long n, long m){
        for (int i = 0; i <n ; i++) {
           if (n == m*i)
               return true;
        }
        return false;
    }

    public static boolean isOdd(int i){
        return !isMultiple(i,2);
    }

    public static  boolean isEven(int i){
        return isMultiple(i,2);
    }

    public static long sumAll(int n){
        long sum =0;
        for (int i = 1; i < n; i++) {
            sum+= i;
        }
        return sum;
    }

    public static long sumOdd(int n){
        long sum =0;
        for (int i = 1; i < n; i++) {
            sum+= i%2 == 0? 0: i;
        }
        return sum;
    }

    /*Créativity section*/

    /**
     * Determines if there is a pair of number in an array taken whose product is odd
     * @param t an array of intergers
     * @return boolean
     */
    public static boolean hasOddProduct(int[] t){
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j!= i && j < t.length; j++) {
                if((t[i]*t[j])%2 != 0)
                    return true;
            }
        }
        return false;
    }

 /**
     * Determines if all the numbers in an array are different each from other
     * @param t an array of intergers
     * @return boolean
     */
    public static boolean hasDistinctValues(int[] t){
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j!= i && j < t.length; j++) {
                if(t[i] == t[j])
                    return false;
            }
        }
        return true;
    }


    /* Create and initialize a table of integers*/
    public int[] initializeTable(int[] t, int length){
        t = new int[length];
        for (int i = 0; i < length; i++) {
            t[i] = i+1;
        }
        return t;
    }


    /**
     * Determines if all the numbers in an array are different each from other
     * @param t an array of intergers
     * @return boolean
     */
    public static boolean shuffleTable(int[] t){
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j!= i && j < t.length; j++) {
                if(t[i] == t[j])
                    return false;
            }
        }
        return true;
    }

    public static void reindex(int[] t){

    }

    public static int giveRandomNumberBetween(int min, int max){
        double[] limits = new double[max];
        int valueToReturn = 0;
        try {
            double limit = (double) 1/max;
            System.out.println("limit:"+limit);

            for (int i = min; i < max; i++) {
                limits[i] = limit + i*limit;
                System.out.println(limits[i]);

            }
            double randomValue = Math.random() < limit * min ? Math.random() + limit * min: Math.random();
            System.out.println("randomValue:"+randomValue);
            for (int i = min; i < max; i++) {
                if(randomValue <= limits[i]){
                    valueToReturn = (int)Math.ceil(randomValue*max);
                }
            }
        }
        catch (Exception e){
            System.out.println("Impossible de diviser par 0");
        }

        return valueToReturn;
    }

    public static int giveRandomNumberfromTable(int[] tab){
        int max =  tab.length;
        double[] limits = new double[max];
        int valueToReturn = 0;

        return valueToReturn;
    }


    public static int[] scalarProduct(int[] a, int[] b, int length) {
        int[] c = new int[length];
        for (int i = 0; i < length; i++) {
            c[i] = a[i] * b[i];
        }
        return c;
    }







    public static void testBoundary(int i) throws ArrayIndexOutOfBoundsException{

        try {
            toto[i] = 4;
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Don't try buffer overflow attacks in Java!");
        }
    }


    public static void main(String[] args) {
        System.out.println(isMultiple(99,3));
        testBoundary(7);
        System.out.println(isOdd(99));
        System.out.println(isEven(99));
        System.out.println(sumAll(99));
        System.out.println(sumOdd(99) );
        System.out.println(hasOddProduct(new int[]{1,2,4,3}));
        System.out.println(hasDistinctValues(new int[]{1,2,4,2}));
        System.out.println(giveRandomNumberBetween(3,4));
        double x = -15.0/0;
        System.out.println("x = " + 15/x);
        boolean b;
        A a = new A();
        System.out.println(a.b?true:false);

    }

    static class A{
        boolean b;

        public void main(String[] args) {

        }
    }
}