package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class StringUtils {


    public static void main(String args[]) throws IOException {

        InputStreamReader is=new InputStreamReader(System.in);
        BufferedReader br=new BufferedReader(is);
        Scanner scanner=new Scanner(System.in);


        List<Character> list = new ArrayList<>();
        String input = "toto a mangé le pain haché";
        input = input.toUpperCase();
        for (char c:input.toCharArray()
             ) {
            if (!list.contains(c))
                list.add(c);
        }
        Collections.sort(list);

        list.forEach(character -> System.out.println( character));

        ///{write your code here
        //String input = scanner.next();
        input = "xbadxx";
        System.out.println(input.startsWith("bad"));
        System.out.println(input.substring(1).startsWith("bad"));
        System.out.println(decodeURL("http://tuto%3Dcom/les%20avantures%20de%20toto%3A"));
    }

    public static String removeVoyelles(String str){
        return str.replaceAll("[aeiou]","");
    }

    /**
     *
     * @param input
     * @return The string input with inversed case.
     */
    public static String InverseCase(String input){
        String lower = input.toLowerCase();
        String upper = input.toUpperCase();
        char[] test = input.toCharArray();
        String result = "";

        for (int i = 0; i <input.length() ; i++) {
            if (input.charAt(i) == lower.charAt(i))
                test[i] = upper.charAt(i);
            else if (input.charAt(i) == upper.charAt(i))
                test[i] = lower.charAt(i);

            result += test[i];
        }
        return result;
    }

    /**
     *Given a string from user, swap the first half with second half.
     * If the string length is odd, take the lower first half and swap.
     * And if the string length is 1, leave it alone.
     * For Example:
     * Input:
     * abcde
     * Output:
     * cdeab
     *
     * @param input
     * @return The string input with inversed case.
     */
    public static String swapHalf(String input){
        int half = input.length()/2;
        String firstHalf = input.substring(0, half);
        String secondHalf = input.substring(half);
        String result = secondHalf+firstHalf;
        return result;
    }

    public static boolean containsBOB(String input){
        return input.matches(".*b(.*)b.*");
    }

    /**
     * Write a Java program that accepts a string as an input. Use the following  method:
     *
     * public static boolean equalIsNot(String str)
     *
     * Following method returns true if the number of appearances of "is" anywhere in the string is equal to the number of appearances of "not" anywhere in the string (case sensitive).
     * @param str
     * @return
     */
    public static boolean equalIsNot(String str){
        int numberOfIs = 0;
        int numberOfNot = 0;
        String chaine = str;
        while (chaine.contains("is")){
            numberOfIs++;
            chaine = chaine.substring(chaine.indexOf("is")+"is".length());
            System.out.println("chaine is= " + chaine);
        }
        System.out.println(numberOfIs);
        chaine = str;
        System.out.println("chaine = " + chaine);
        while (chaine.contains("not")){
            numberOfNot++;
            chaine = chaine.substring(chaine.indexOf("not")+"not".length());
            System.out.println("chaine not= " + chaine);
        }
        System.out.println(numberOfNot);
        return numberOfIs == numberOfNot;
    }

    /**
     *Write a Java program that accepts a string as an input. Use the following method:
     *
     * public static String notReplace(String str)
     *
     * Following method must return a string where every appearance of the lowercase word "is" has been replaced with "is not".
     * The string "is" should not be immediately preceeded or followed by a letter.
     * @param str
     * @return
     */
    public static String notReplace(String str){
        String result = "";
        str.indexOf("is");
        boolean flag = false;
        if (!str.startsWith("is") || flag)
            result = str.replaceAll("-is|:is|,is|;is |\\.is ","is not");
        else {
            result = str.replace("^is ", "is not ");
            flag = true;
        }
        return result;
    }


    /**
     * Write a java program to modify your input in place. Replace the substring with following characters as shown below:
     *
     * %20 -> ' '
     * %3A -> '?'
     * %3D -> '.'
     * @param s
     * @return
     */
    public static String decodeURL(String s){
        s = s.replaceAll("%20"," ");
       s =  s.replaceAll("%3A","?");
        s = s.replaceAll("%3D",".");
        return s;
    }

}
