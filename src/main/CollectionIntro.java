package main;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionIntro {

    public static void main(String[] args) {
        int t[] = new int[]{3, 4, 5, 6,0B100_00_00000};
        for (int i = 0; i < t.length; i++) {
            System.out.println(t[i]);
        }

        System.out.println("Paramètre de ligne de commande");
        for (String arg : args) {
            System.out.println(arg);
        }
        Collection values = new ArrayList();
        values.add(3);
        values.add(5);
        values.add("toto");
        values.add(true);
        values.add(12.2);
        values.add(values);

        for (Object value: values) {
            System.out.println(value);
        }

        values.forEach(e-> System.out.println(e.getClass().getName()));

        A a = new A();
        A b = a;
        b.setA(5);
        a.Affiche();

    }

    public static class A{
        public int a = 4;

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }

        public void Affiche(){
            System.out.println(a);
        }
    }
}


class ListIntro extends CollectionIntro{
    ListIntro(){
        super();

    }
}
