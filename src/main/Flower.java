package main;

import java.util.Objects;

public class Flower {

    private String name;
    private int numberOfPetal;
    private double price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flower)) return false;
        Flower flower = (Flower) o;
        return numberOfPetal == flower.numberOfPetal &&
                Double.compare(flower.price, price) == 0 &&
                name.equals(flower.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, numberOfPetal, price);
    }

    public Flower(String name, int numberOfPetal, double price) {
        this.name = name;
        this.numberOfPetal = numberOfPetal;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPetal() {
        return numberOfPetal;
    }

    public void setNumberOfPetal(int numberOfPetal) {
        this.numberOfPetal = numberOfPetal;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
