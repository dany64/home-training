package main;

public class OODesign {

    public static void main(String[] args) {
        Horse d = new Equestrian();
        Equestrian r = (Equestrian) d;
    }
}

class Goat extends Object{
    private int tails;

    public void milk(){}
    public void jump(){}
}

class Pig extends Object{
    private int nose;

    public void eat(){}
    public void wallow(){}
}

class Horse extends Object{
    private int height;
    private int color;

    public void run(){}
    public void jump(){}
}

class Racer extends Horse{
    public void race(){}
}

class Equestrian extends Horse{
    private int weight;

    public void trot(){}
    public void trained(){}
}


