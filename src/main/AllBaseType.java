package main;

import java.util.Scanner;

public class AllBaseType {

    public void inputAllBasicType(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an integer: ");
        int i = scanner.nextInt();
        System.out.println(i);

        System.out.print("Enter a byte: ");
        byte c = scanner.nextByte();
        System.out.println(c);

        System.out.print("Enter a float: ");
        float f = scanner.nextFloat();
        System.out.println(f);

        System.out.print("Enter an double: ");
        double d = scanner.nextDouble();
        System.out.println(d);

        System.out.print("Enter a boolean: ");
        boolean b = scanner.nextBoolean();
        System.out.println(b);

        System.out.print("Enter a String: ");
        String s = scanner.next();
        System.out.println(s);

    }

    public static void main(String[] args) {
        AllBaseType abt = new AllBaseType();
        abt.inputAllBasicType();
    }
}
